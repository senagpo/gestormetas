<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['guest']], function () {
    Route::post('login', 'AuthController@signIn');
    Route::get('isguest', function () {
        return response()->json(['valid' => true]);
    });
});
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::group(['middleware' => ['auth']], function () {

        Route::get('currentUser', 'UserController@fetchUser');

        Route::get('logout', 'AuthController@signOut');

        Route::post('changePass', 'AuthController@changePassword');

        // Training Center Related Routes
        Route::group(['prefix' => 'trainingCenter'], function () {
            Route::get('all', 'TrainingCenterController@allTrainingCenters')->middleware('role:reguser');
            Route::get('population', 'TrainingCenterController@populationsRemaining')->middleware('role:reguser');
            Route::get('technic', 'TrainingCenterController@technicRemaining')->middleware('role:reguser');
            Route::get('formation', 'TrainingCenterController@formationRemaining')->middleware('role:reguser');
        });

        // Vulnerable Populations Related Routes
        Route::group(['prefix' => 'vulnerablePopulation'], function () {
            Route::get('all', 'VulnerablePopulationController@index')->middleware('role:reguser');
        });

        // Goals Related Routes
        Route::group(['prefix' => 'goal'], function () {
            Route::post('uploadData', 'GoalController@importGoals')->middleware('role:admin,manager');

            Route::group(['middleware' => ['role:reguser']], function () {
                // Vulnerable Populations related Routes
                Route::get('population/regional', 'GoalController@retrieveRegionalPopulationGoals');
                Route::get('population/center', 'GoalController@retrieveCenterPopulationGoals');
                Route::post('population/regional/store', 'GoalController@storeRegionalPopulationGoals');
                Route::post('population/center/store', 'QuotaController@storePopulationQuotas');
                Route::get('population/regional/isdis', 'GoalController@isRegionalPopulationDis');

                // Technical related Routes
                Route::get('technical/{id}/center', 'GoalController@retrieveTechnicalGoal');
                Route::get('technical/{id}/center/quotas', 'QuotaController@retrieveTechnicalQuotas');
                Route::post('technical/{id}/center/store', 'QuotaController@storeTechnicalQuotas');

                // Formation related Routes
                Route::get('formation/{id}/center', 'GoalController@retrieveFormationGoal');
                Route::get('formation/{id}/center/quotas', 'QuotaController@retrieveFormationQuotas');
                Route::post('formation/{id}/center/store', 'QuotaController@storeFormationQuotas');
            });
        });

        // Budget Related routes
        Route::group(['prefix' => 'budget'], function () {
            Route::post('{id}/store', 'BudgetController@storeBudget');
        });

        // Exports Related routes
        Route::group(['prefix' => 'exports', 'middleware' => ['role:admin,manager']], function () {
            Route::group(['prefix' => 'quotas'], function () {
                Route::get('all', 'ExportsController@downloadGeneral');
            });
        });

        // Validities Related Routes
        Route::group(['prefix' => 'validities', 'middleware' => ['role:admin']], function () {
            Route::get('all', 'ValidityController@all');
            Route::get('{id}/change', 'ValidityController@changeValidity');
        });


        // Role Checking Routes
        Route::get('isauth', function () {
            return response()->json(['valid' => true]);
        });

        Route::get('isadmin', function () {
            return response()->json(['valid' => true]);
        })->middleware('role:admin');


        Route::get('ismanager', function () {
            return response()->json(['valid' => true]);
        })->middleware('role:admin,manager');


        Route::get('isregional', function () {
            return response()->json(['valid' => true]);
        })->middleware('role:reguser');
    });
});
