<?php

namespace App\Imports;

use App\Models\Goal;
use App\Models\GoalType;
use App\Models\RegionalEntity;
use App\Models\State;
use App\Models\Validity;
use App\Models\VulnerablePopulation;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Str;

class GoalsImport implements WithHeadingRow, WithStrictNullComparison, WithCalculatedFormulas, ToCollection
{

    use Importable;

    protected $target;

    public function __construct(string $target)
    {
        $this->target = $target;
    }

    /**
     *  Returns the row that contains the
     *  headings by default 7 row
     *
     *  @return int headingRow
     */
    public function headingRow(): int
    {
        return 7;
    }


    /**
     *  Create vulnerable population goals
     *  from the imported file
     *
     *  @return void
     */
    protected function publishPopulationGoals(Collection $rows): void
    {
        try {


            $regionalEntities = RegionalEntity::all();
            $validity = Validity::currentLoading();
            $keyTotalApprentices = "metas_{$validity->start_date->year}_otras_poblaciones_aprendices";
            $keyTotalPlaces = "metas_{$validity->start_date->year}_otras_poblaciones_cupos";
            $predifinedPopulations = VulnerablePopulation::orWhere('name', 'Discapacitados')
                ->orWhere('name', 'Negritudes')->orWhere('name', 'Afrocolombianos')
                ->orWhere('name', 'Raizales')->orWhere('name',  'Palenqueros')
                ->get(['name', 'id']);

            foreach ($regionalEntities as $regionalEntity) {
                $trimedCode = ltrim($regionalEntity->code, '0');

                Goal::updateOrCreate([
                    'regional_entity_id' => $regionalEntity->id,
                    'goal_type_id' => GoalType::firstWhere('name', 'Meta Regional para Poblacion Vulnerable')->id,
                    'vulnerable_population_id' => null,
                    'validity_id' => $validity->id
                ], [
                    'apprentices' => $rows->firstWhere('cod_regional', $trimedCode)->get($keyTotalApprentices),
                    'places' => $rows->firstWhere('cod_regional', $trimedCode)->get($keyTotalPlaces),
                    'state_id' => State::firstWhere('name', 'Sin Desagregar')->id
                ]);

                foreach ($predifinedPopulations as $predifinedPopulation) {
                    $slugName = Str::slug($predifinedPopulation->name, '_');
                    $keyPredifinedApprentices = "metas_{$validity->start_date->year}_{$slugName}_aprendices";
                    $keyPredifinedPlaces = "metas_{$validity->start_date->year}_{$slugName}_cupos";

                    Goal::updateOrCreate([
                        'regional_entity_id' => $regionalEntity->id,
                        'goal_type_id' => GoalType::firstWhere('name', 'Meta Regional para Poblacion Vulnerable')->id,
                        'vulnerable_population_id' => $predifinedPopulation->id,
                        'validity_id' => $validity->id
                    ], [
                        'apprentices' => $rows->firstWhere('cod_regional', $trimedCode)->get($keyPredifinedApprentices),
                        'places' => $rows->firstWhere('cod_regional', $trimedCode)->get($keyPredifinedPlaces),
                        'state_id' => State::firstWhere('name', 'Sin Desagregar')->id
                    ]);
                }
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     *  Create schools goals
     *  from the imported file
     */
    protected function publishSchoolGoals(Collection $rows): void
    {
        try {
            $regionalEntities = RegionalEntity::all();
            $validity = Validity::firstWhere('can_load', true);
            $keyTotalApprentices = "metas_{$validity->start_date->year}_aprendices_tecnico_lab_articul";
            $keyTotalPlaces = "metas_{$validity->start_date->year}_cupos_tecnico_lab_articul";
            foreach ($regionalEntities as $regionalEntity) {
                $trimedCode = ltrim($regionalEntity->code, '0');
                Goal::updateOrCreate([
                    'regional_entity_id' => $regionalEntity->id,
                    'goal_type_id' => GoalType::firstWhere('name', 'Meta Regional para Articulacion con la Media')->id,
                    'validity_id' => $validity->id
                ], [
                    'apprentices' => $rows->where('cod_reg', $trimedCode)->sum($keyTotalApprentices),
                    'places' => $rows->where('cod_reg', $trimedCode)->sum($keyTotalPlaces),
                    'state_id' => State::firstWhere('name', 'Sin Desagregar')->id
                ]);

                $trainingCenters = $regionalEntity->trainingCenters;
                foreach ($trainingCenters as $trainingCenter) {
                    Goal::updateOrCreate([
                        'training_center_id' => $trainingCenter->id,
                        'goal_type_id' => GoalType::firstWhere('name', 'Meta Centro para Articulacion con la media')->id,
                        'validity_id' => $validity->id
                    ], [
                        'apprentices' => $rows->where('codigo_centro', $trainingCenter->code)->sum($keyTotalApprentices),
                        'places' => $rows->where('codigo_centro', $trainingCenter->code)->sum($keyTotalPlaces),
                        'state_id' => State::firstWhere('name', 'Sin Desagregar')->id
                    ]);
                    Goal::updateOrCreate([
                        'regional_entity_id' => $trainingCenter->regionalEntity->id,
                        'goal_type_id' => GoalType::firstWhere('name', 'Meta Regional para Articulacion con la Media')->id,
                        'validity_id' => $validity->id
                    ], [
                        'state_id' => State::firstWhere('name', 'Desagregado')->id
                    ]);
                }
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     *  Create formation goals
     *  from the imported file
     */
    protected function publishFormationGoals(Collection $rows): void
    {
        try {
            $regionalEntities = RegionalEntity::all();
            $validity = Validity::firstWhere('can_load', true);
            $keyTotalApprentices = "metas_{$validity->start_date->year}_aprendices_total_formacion_titulada";
            $keyTotalPlaces = "metas_{$validity->start_date->year}_cupos_total_formacion_titulada";
            foreach ($regionalEntities as $regionalEntity) {
                $trimedCode = ltrim($regionalEntity->code, '0');
                Goal::updateOrCreate([
                    'regional_entity_id' => $regionalEntity->id,
                    'goal_type_id' => GoalType::firstWhere('name', 'Meta Regional para Formacion Titulada')->id,
                    'validity_id' => $validity->id
                ], [
                    'apprentices' => $rows->where('cod_reg', $trimedCode)->sum($keyTotalApprentices),
                    'places' => $rows->where('cod_reg', $trimedCode)->sum($keyTotalPlaces),
                    'state_id' => State::firstWhere('name', 'Sin Desagregar')->id
                ]);

                $trainingCenters = $regionalEntity->trainingCenters;
                foreach ($trainingCenters as $trainingCenter) {
                    Goal::updateOrCreate([
                        'training_center_id' => $trainingCenter->id,
                        'goal_type_id' => GoalType::firstWhere('name', 'Meta Centro para Formacion Titulada')->id,
                        'validity_id' => $validity->id
                    ], [
                        'apprentices' => $rows->where('codigo_centro', $trainingCenter->code)->sum($keyTotalApprentices),
                        'places' => $rows->where('codigo_centro', $trainingCenter->code)->sum($keyTotalPlaces),
                        'state_id' => State::firstWhere('name', 'Sin Desagregar')->id
                    ]);
                    Goal::updateOrCreate([
                        'regional_entity_id' => $trainingCenter->regionalEntity->id,
                        'goal_type_id' => GoalType::firstWhere('name', 'Meta Regional para Formacion Titulada')->id,
                        'validity_id' => $validity->id
                    ], [
                        'state_id' => State::firstWhere('name', 'Desagregado')->id
                    ]);
                }
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     *  Handle imported file rows
     *  into collection processable
     *
     *  @return void
     */
    public function collection(Collection $rows): void
    {

        try {
            switch ($this->target) {
                case 'populations':
                    $this->publishPopulationGoals($rows);
                    break;

                case 'schools':
                    $this->publishSchoolGoals($rows);
                    break;

                case 'formation':
                    $this->publishFormationGoals($rows);
                    break;

                default:
                    if (env('APP_DEBUG', false)) lad('not available');
                    break;
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
