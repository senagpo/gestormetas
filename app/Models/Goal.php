<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'places', 'apprentices', 'goal_type_id', 'regional_entity_id', 'vulnerable_population_id',
        'training_center_id', 'validity_id', 'state_id'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'places' => 'integer',
        'apprentices' => 'integer',
    ];

    public function regionalEntity()
    {
        return $this->belongsTo(RegionalEntity::class);
    }

    public function trainingCenter()
    {
        return $this->belongsTo(TrainingCenter::class);
    }

    public function vulnerablePopulation()
    {
        return $this->belongsTo(VulnerablePopulation::class);
    }

    public function goalType()
    {
        return $this->belongsTo(GoalType::class);
    }

    public function validity()
    {
        return $this->belongsTo(Validity::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function quotas()
    {
        return $this->hasMany(Quota::class);
    }
}
