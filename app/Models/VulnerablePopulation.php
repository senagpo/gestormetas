<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VulnerablePopulation extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function goals()
    {
        return $this->hasMany(Goal::class);
    }

    public function quotas()
    {
        return $this->hasMany(Quota::class);
    }
}
