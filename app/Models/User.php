<?php

namespace App\Models;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'state_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     *  Check if has roles
     *
     * @param string[] $roles
     * @return bool
     */
    public function hasRole($roles)
    {
        $valid = false;
        foreach ($roles as $role) {
            if ($this->role->name === $role) {
                $valid = true;
                break;
            }
        }
        return $valid;
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function regionalEntity()
    {
        return $this->hasOne(RegionalEntity::class);
    }
}
