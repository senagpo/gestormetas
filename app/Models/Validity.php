<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Validity extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'duration', 'start_date', 'end_date',
        'can_load', 'can_view'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'duration' => 'integer',
        'start_date' => 'date',
        'end_date' => 'date',
        'can_load' => 'boolean',
        'can_view' => 'boolean'
    ];

    /**
     *  Returns the active validity
     */
    public static function currentLoading()
    {
        return Validity::firstWhere('can_load', true);
    }

    /**
     *  Returns the active view validities
     */
    public static function currentSearchable()
    {
        return Validity::where('can_view', true);
    }

    public function goals()
    {
        return $this->hasMany(Goal::class);
    }

    public function quotas()
    {
        return $this->hasMany(Quota::class);
    }

    public function budgets()
    {
        return $this->hasMany(Budget::class);
    }
}
