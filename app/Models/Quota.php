<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quota extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'places_amount', 'apprentices_amount', 'quota_type_id', 'training_center_id',
        'validity_id', 'vulnerable_population_id', 'goal_id'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'places_amount' => 'integer',
        'apprentices_amount' => 'integer',
    ];

    public function goal()
    {
        return $this->belongsTo(Goal::class);
    }

    public function trainingCenter()
    {
        return $this->belongsTo(TrainingCenter::class);
    }

    public function vulnerablePopulation()
    {
        return $this->belongsTo(VulnerablePopulation::class);
    }

    public function validity()
    {
        return $this->belongsTo(Validity::class);
    }

    public function quotaType()
    {
        return $this->belongsTo(QuotaType::class);
    }
}
