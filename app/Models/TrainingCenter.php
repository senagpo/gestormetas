<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrainingCenter extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name', 'regional_entity_id', 'state_id'
    ];

    public function regionalEntity()
    {
        return $this->belongsTo(RegionalEntity::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function goals()
    {
        return $this->hasMany(Goal::class);
    }

    public function quotas()
    {
        return $this->hasMany(Quota::class);
    }

    public function budgets()
    {
        return $this->hasMany(Budget::class);
    }
}
