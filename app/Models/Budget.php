<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'requested_amount', 'validity_id',
        'training_center_id', 'budget_type_id'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'requested_amount' => 'integer',
    ];

    public function budgetType()
    {
        return $this->belongsTo(BudgetType::class);
    }

    public function validity()
    {
        return $this->belongsTo(Validity::class);
    }

    public function trainingCenter()
    {
        return $this->belongsTo(TrainingCenter::class);
    }
}
