<?php

namespace App\Http\Middleware;

use Closure;
use Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful as MiddlewareEnsureFrontendRequestsAreStateful;

class EnsureFrontendRequestsAreStateful extends MiddlewareEnsureFrontendRequestsAreStateful
{
    /**
     * Configure secure cookie sessions.
     *
     * @return void
     */
    protected function configureSecureCookieSessions()
    {
        config([
            'session.http_only' => false,
            'session.same_site' => 'lax',
        ]);
    }
}
