<?php

namespace App\Http\Controllers;

use App\Models\Goal;
use App\Models\Quota;
use App\Models\QuotaType;
use App\Models\State;
use App\Models\TrainingCenter;
use App\Models\Validity;
use Illuminate\Http\Request;

class QuotaController extends Controller
{
    /**
     *  Stores all center quota
     *  in the current user context
     *
     *  @param Request $request
     *  @return \Illuminate\Http\JsonResponse
     */
    public function storePopulationQuotas(Request $request)
    {
        try {
            $trainingCenter = $request->center;
            $centerQuotas = $request->centerQuotas;
            lad($centerQuotas);
            foreach ($centerQuotas as $quota) {

                if ($quota['goal'] !== 0) {

                    $apprentices = isset($quota['apprentices']) ? intval($quota['apprentices']) : 0;
                    $places = isset($quota['places']) ? intval($quota['places']) : 0;

                    $goal = Goal::findOrFail($quota['goal']);
                    $goal->apprentices = ($goal->apprentices - $apprentices);
                    $goal->places = ($goal->places - $places);
                    $goal->state_id = State::firstWhere('name', 'Desagregado')->id;
                    $goal->saveOrFail();


                    Quota::updateOrCreate([
                        'quota_type_id' => QuotaType::firstWhere('name', 'Cuota de poblacion vulnerable')->id,
                        'vulnerable_population_id' => $quota['population'],
                        'training_center_id' => $trainingCenter['id'],
                        'goal_id' =>  $quota['goal'],
                        'validity_id' => Validity::currentLoading()->id
                    ], [
                        'apprentices_amount' => $apprentices,
                        'places_amount' => $places,
                    ]);
                }
            }

            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'error' => $th->getMessage()
            ], 500);
            throw $th;
        }
    }

    /**
     *  Retrieves existent technical quotas
     *
     *  @return \Illuminate\Http\JsonResponse
     */
    public function retrieveTechnicalQuotas(int $id)
    {
        try {

            $types = QuotaType::whereIn('name', [
                'Cuota de Articulacion con la Media Nuevos',
                'Cuota de Articulacion con la Media Pasan', 'Cuota de Articulacion con la Media Nuevos Academica',
                'Cuota de Articulacion con la Media Nuevos Tecnica', 'Cuota de Articulacion con la Media Nuevos Privada',
                'Cuota de Articulacion con la Media Pasan Academica', 'Cuota de Articulacion con la Media Pasan Tecnica',
                'Cuota de Articulacion con la Media Pasan Privada'
            ])->pluck('id')->toArray();

            $quotas = Quota::where('training_center_id', $id)
                ->whereIn('quota_type_id', $types)
                ->get();
            if (isset($quotas)) {
                foreach ($quotas as $quota) {
                    $quota->quota_type_name = QuotaType::find($quota->quota_type_id)->name;
                }
            }
            return response()->json(['success' => true, 'quotas' => $quotas]);
        } catch (\Throwable $th) {
            throw $th;
            return response()->json([
                'success' => false,
                'quotas' => null,
                'error' => "Line: {$th->getLine()}, {$th->getMessage()}"
            ], 500);
        }
    }

    /**
     *  Retrieves existent formation quotas
     *
     *  @return \Illuminate\Http\JsonResponse
     */
    public function retrieveFormationQuotas(int $id)
    {
        try {

            $types = QuotaType::whereIn('name', [
                'Cuota de Formacion Titulada Pasan', 'Cuota de Formacion Titulada Nuevos',
                'Cuota de Formacion Titulada Nuevos FIC', 'Cuota de Formacion Titulada Pasan FIC',
                'Cuota de Formacion Titulada Nuevos FIC T.I', 'Cuota de Formacion Titulada Nuevos FIC T.II',
                'Cuota de Formacion Titulada Nuevos FIC T.III', 'Cuota de Formacion Titulada Nuevos FIC T.IV',
                'Cuota de Formacion Titulada Nuevos Regular', 'Cuota de Formacion Titulada Pasan Regular',
                'Cuota de Formacion Titulada Nuevos Regular T.I', 'Cuota de Formacion Titulada Nuevos Regular T.II',
                'Cuota de Formacion Titulada Nuevos Regular T.III', 'Cuota de Formacion Titulada Nuevos Regular T.IV',
            ])->pluck('id')->toArray();

            $quotas = Quota::where('training_center_id', $id)
                ->whereIn('quota_type_id', $types)
                ->get();
            if (isset($quotas)) {
                foreach ($quotas as $quota) {
                    $quota->quota_type_name = QuotaType::find($quota->quota_type_id)->name;
                }
            }
            return response()->json(['success' => true, 'quotas' => $quotas]);
        } catch (\Throwable $th) {
            throw $th;
            return response()->json([
                'success' => false,
                'quotas' => null,
                'error' => "Line: {$th->getLine()}, {$th->getMessage()}"
            ], 500);
        }
    }


    /**
     *  Store the technical formation quotas
     *
     *  @param int $id training center ID
     *  @return \Illuminate\Http\JsonResponse
     */
    public function storeTechnicalQuotas(int $id, Request $request)
    {
        try {
            $trainingCenter = TrainingCenter::findOrFail($id);
            $goal = Goal::findOrFail($request->goal);
            $quotas = $request->centerQuotas;
            $goal->state_id = State::firstWhere('name', 'Desagregado')->id;
            $goal->saveOrFail();

            $types = collect([
                'new' => [
                    'total' =>  QuotaType::firstWhere('name', 'Cuota de Articulacion con la Media Nuevos')->id,
                    'academic' => QuotaType::firstWhere('name', 'Cuota de Articulacion con la Media Nuevos Academica')->id,
                    'technic' => QuotaType::firstWhere('name', 'Cuota de Articulacion con la Media Nuevos Tecnica')->id,
                    'private' => QuotaType::firstWhere('name', 'Cuota de Articulacion con la Media Nuevos Privada')->id
                ],
                'pass' => [
                    'total' => QuotaType::firstWhere('name', 'Cuota de Articulacion con la Media Pasan')->id,
                    'academic' => QuotaType::firstWhere('name', 'Cuota de Articulacion con la Media Pasan Academica')->id,
                    'technic' => QuotaType::firstWhere('name', 'Cuota de Articulacion con la Media Pasan Tecnica')->id,
                    'private' => QuotaType::firstWhere('name', 'Cuota de Articulacion con la Media Pasan Privada')->id
                ]
            ]);

            foreach ($types as $type => $subtypes) {
                $quota = $quotas[$type];
                foreach ($subtypes as $kind => $id) {
                    $apprentices = isset($quota[$kind]['apprentices']) ? intval($quota[$kind]['apprentices']) : 0;
                    $places = isset($quota[$kind]['places']) ? intval($quota[$kind]['places']) : 0;

                    Quota::updateOrCreate([
                        'quota_type_id' => $id,
                        'training_center_id' => $trainingCenter->id,
                        'goal_id' => $goal->id,
                        'validity_id' => Validity::currentLoading()->id
                    ], [
                        'apprentices_amount' => $apprentices,
                        'places_amount' => $places,
                    ]);
                }
            }

            return response()->json(['success' => true]);
        } catch (\Throwable $th) {
            throw $th;
            return response()->json([
                'success' => false,
                'error' => "Line: {$th->getLine()}, {$th->getMessage()}"
            ], 500);
        }
    }

    /**
     *  Store the formation formation quotas
     *
     *  @param int $id training center ID
     *  @return \Illuminate\Http\JsonResponse
     */
    public function storeFormationQuotas(int $id, Request $request)
    {
        try {
            $trainingCenter = TrainingCenter::findOrFail($id);
            $goal = Goal::findOrFail($request->goal);
            $quotas = $request->centerQuotas;
            $goal->state_id = State::firstWhere('name', 'Desagregado')->id;
            $goal->saveOrFail();

            $types = collect([
                'new' => [
                    'total' =>  QuotaType::firstWhere('name', 'Cuota de Formacion Titulada Nuevos')->id,
                    'fic' => [
                        'base' => QuotaType::firstWhere('name', 'Cuota de Formacion Titulada Nuevos FIC')->id,
                        'ti' => QuotaType::firstWhere('name', 'Cuota de Formacion Titulada Nuevos FIC T.I')->id,
                        'tii' => QuotaType::firstWhere('name', 'Cuota de Formacion Titulada Nuevos FIC T.II')->id,
                        'tiii' => QuotaType::firstWhere('name', 'Cuota de Formacion Titulada Nuevos FIC T.III')->id,
                        'tiv' => QuotaType::firstWhere('name', 'Cuota de Formacion Titulada Nuevos FIC T.IV')->id
                    ],
                    'regular' => [
                        'base' => QuotaType::firstWhere('name', 'Cuota de Formacion Titulada Nuevos Regular')->id,
                        'ti' => QuotaType::firstWhere('name', 'Cuota de Formacion Titulada Nuevos Regular T.I')->id,
                        'tii' => QuotaType::firstWhere('name', 'Cuota de Formacion Titulada Nuevos Regular T.II')->id,
                        'tiii' => QuotaType::firstWhere('name', 'Cuota de Formacion Titulada Nuevos Regular T.III')->id,
                        'tiv' => QuotaType::firstWhere('name', 'Cuota de Formacion Titulada Nuevos Regular T.IV')->id
                    ],
                ],
                'pass' => [
                    'total' => QuotaType::firstWhere('name', 'Cuota de Formacion Titulada Pasan')->id,
                    'fic' => QuotaType::firstWhere('name', 'Cuota de Formacion Titulada Pasan FIC')->id,
                    'regular' => QuotaType::firstWhere('name', 'Cuota de Formacion Titulada Pasan Regular')->id,
                ]
            ]);

            foreach ($types as $type => $subtypes) {
                $quota = $quotas[$type];

                if ($type === 'pass') {
                    foreach ($subtypes as $kind => $id) {
                        Quota::updateOrCreate([
                            'quota_type_id' => $id,
                            'training_center_id' => $trainingCenter->id,
                            'goal_id' => $goal->id,
                            'validity_id' => Validity::currentLoading()->id
                        ], [
                            'apprentices_amount' => 0,
                            'places_amount' => isset($quota[$kind]['places']) ? intval($quota[$kind]['places']) : 0,
                        ]);
                    }
                } elseif ($type === 'new') {
                    foreach ($subtypes as $kind => $value) {
                        if ($kind === 'total')
                            Quota::updateOrCreate([
                                'quota_type_id' => $value,
                                'training_center_id' => $trainingCenter->id,
                                'goal_id' => $goal->id,
                                'validity_id' => Validity::currentLoading()->id
                            ], [
                                'apprentices_amount' => 0,
                                'places_amount' => isset($quota[$kind]['places']) ? intval($quota[$kind]['places']) : 0,
                            ]);
                        else {
                            foreach ($value as $period => $periodValue) {
                                if ($period === 'base')
                                    Quota::updateOrCreate([
                                        'quota_type_id' => $periodValue,
                                        'training_center_id' => $trainingCenter->id,
                                        'goal_id' => $goal->id,
                                        'validity_id' => Validity::currentLoading()->id
                                    ], [
                                        'apprentices_amount' => 0,
                                        'places_amount' => isset($quota[$kind]['places']) ? intval($quota[$kind]['places']) : 0,
                                    ]);
                                else
                                    Quota::updateOrCreate([
                                        'quota_type_id' => $periodValue,
                                        'training_center_id' => $trainingCenter->id,
                                        'goal_id' => $goal->id,
                                        'validity_id' => Validity::currentLoading()->id
                                    ], [
                                        'apprentices_amount' => 0,
                                        'places_amount' => isset($quota[$kind][$period]['places']) ? intval($quota[$kind][$period]['places']) : 0,
                                    ]);
                            }
                        }
                    }
                }
            }

            return response()->json(['success' => true]);
        } catch (\Throwable $th) {
            throw $th;
            return response()->json([
                'success' => false,
                'error' => "Line: {$th->getLine()}, {$th->getMessage()}"
            ], 500);
        }
    }
}
