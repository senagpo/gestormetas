<?php

namespace App\Http\Controllers;

use App\Imports\GoalsImport;
use App\Models\Goal;
use App\Models\GoalType;
use App\Models\RegionalEntity;
use App\Models\State;
use App\Models\Validity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class GoalController extends Controller
{
    /**
     *  Import all goals from file upload
     *
     *  @return \Illuminate\Http\JsonResponse
     */
    public function importGoals(Request $request)
    {

        try {
            $file = $request->file('matrix');
            $target = $request->target;
            if (isset($file) && isset($target)) {
                Excel::import(new GoalsImport($target), $file);
                return response()->json(['success' => true]);
            }
            return response()->json(['success' => false]);
        } catch (\Throwable $th) {
            return response()->json(['success' => false, 'error' => $th->getMessage()], 500);
            throw $th;
        }
    }

    /**
     *  Retrieves all regional goals
     *  in the current user context
     *
     *  @return \Illuminate\Http\JsonResponse
     */
    public function retrieveRegionalPopulationGoals()
    {
        try {
            $validity = Validity::currentLoading();
            $regionalEntity = Auth::user()->regionalEntity;
            $regionalGoal = Goal::firstWhere('goal_type_id', GoalType::firstWhere('name', 'Meta Regional para Poblacion Vulnerable')->id)
                ->where('regional_entity_id', $regionalEntity->id)
                ->where('validity_id', $validity->id)
                ->get();


            return response()->json([
                'success' => true,
                'regionalGoals' => $regionalGoal,
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false, 'regionalGoals' => null,
                'error' => $th->getMessage()
            ], 500);
            throw $th;
        }
    }

    /**
     *  Retrieves all regional goals
     *  in the current user context
     *
     *  @return \Illuminate\Http\JsonResponse
     */
    public function retrieveCenterPopulationGoals()
    {
        try {
            $validity = Validity::currentLoading();
            $regionalEntity = Auth::user()->regionalEntity;
            $centerGoals = Goal::where('goal_type_id', GoalType::firstWhere('name', 'Meta Centro para Poblacion Vulnerable')->id)
                ->where('regional_entity_id', $regionalEntity->id)
                ->where('validity_id', $validity->id)
                ->get();


            return response()->json([
                'success' => true,
                'centerGoals' => $centerGoals,
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false, 'centerGoals' => null,
                'error' => $th->getMessage()
            ], 500);
            throw $th;
        }
    }

    /**
     *  Retrieves the regional population results
     *  if is already disaggregated true else false
     *
     *  this avoids redudancy in the process
     *
     *  @return \Illuminate\Http\JsonResponse
     */
    public function isRegionalPopulationDis()
    {
        try {
            $validity = Validity::currentLoading();
            $regionalEntity = Auth::user()->regionalEntity;
            $goals = Goal::where('goal_type_id', GoalType::firstWhere('name', 'Meta Centro para Poblacion Vulnerable')->id)
                ->where('regional_entity_id', $regionalEntity->id)
                ->where('validity_id', $validity->id)
                ->where('vulnerable_population_id', '!=', null)
                ->get();

            if (isset($goals) && $goals->count() > 0)
                return response()->json(['isdisaggregated' => true]);
            else
                return response()->json(['isdisaggregated' => false]);
        } catch (\Throwable $th) {
            return response()->json([
                'isdisaggregated' => false,
                'error' => $th->getMessage()
            ], 500);
            throw $th;
        }
    }

    /**
     *  Stores all regional goals
     *  in the current user context
     *
     *  @param Request $request
     *  @return \Illuminate\Http\JsonResponse
     */
    public function storeRegionalPopulationGoals(Request $request)
    {

        try {
            $regionalGoals = $request->regionalGoals;
            $regionalEntity = Auth::user()->regionalEntity;
            $validity = Validity::currentLoading();
            foreach ($regionalGoals as $regionalGoal) {

                if ($regionalGoal['goal'] !== 0) {

                    Goal::updateOrCreate([
                        'regional_entity_id' => $regionalEntity->id,
                        'goal_type_id' => GoalType::firstWhere('name', 'Meta Centro para Poblacion Vulnerable')->id,
                        'vulnerable_population_id' => $regionalGoal['population'],
                        'validity_id' => $validity->id
                    ], [
                        'apprentices' => $regionalGoal['apprentices'],
                        'places' => $regionalGoal['places'],
                        'state_id' => State::firstWhere('name', 'Sin Desagregar')->id
                    ]);
                }
            }

            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'error' => $th->getMessage()
            ], 500);
            throw $th;
        }
    }

    /**
     *  Retrieves the requested center
     *  technical goal
     *
     *  @return \Illuminate\Http\JsonResponse
     */
    public function retrieveTechnicalGoal(int $id)
    {
        try {
            $goal = Goal::where('training_center_id', $id)
                ->where('goal_type_id', GoalType::firstWhere('name', 'Meta Centro para Articulacion con la media')->id)
                ->first();
            if (isset($goal))
                return response()->json(['success' => true, 'goal' => $goal]);
            else
                return response()->json(['success' => false, 'goal' => null]);
        } catch (\Throwable $th) {
            throw $th;
            return response()->json([
                'success' => false,
                'goal' => null,
                'error' => "Line: {$th->getLine()}, {$th->getMessage()}"
            ], 500);
        }
    }

    /**
     *  Retrieves the requested center
     *  formation goal
     *
     *  @return \Illuminate\Http\JsonResponse
     */
    public function retrieveFormationGoal(int $id)
    {
        try {
            $goal = Goal::where('training_center_id', $id)
                ->where('goal_type_id', GoalType::firstWhere('name', 'Meta Centro para Formacion Titulada')->id)
                ->first();
            if (isset($goal))
                return response()->json(['success' => true, 'goal' => $goal]);
            else
                return response()->json(['success' => false, 'goal' => null]);
        } catch (\Throwable $th) {
            throw $th;
            return response()->json([
                'success' => false,
                'goal' => null,
                'error' => "Line: {$th->getLine()}, {$th->getMessage()}"
            ], 500);
        }
    }
}
