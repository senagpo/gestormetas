<?php

namespace App\Http\Controllers;

use App\Models\VulnerablePopulation;
use Illuminate\Http\Request;

class VulnerablePopulationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $vulnerablePopulations = VulnerablePopulation::all();
            return response()->json(['success' => true, 'vulnerablePopulations' => $vulnerablePopulations]);
        } catch (\Throwable $th) {
            return response()->json(['success' => false, 'vulnerablePopulations' => null, 'error' => $th->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Model\VulnerablePopulation  $vulnerablePopulation
     * @return \Illuminate\Http\Response
     */
    public function show(VulnerablePopulation $vulnerablePopulation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Model\VulnerablePopulation  $vulnerablePopulation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VulnerablePopulation $vulnerablePopulation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Model\VulnerablePopulation  $vulnerablePopulation
     * @return \Illuminate\Http\Response
     */
    public function destroy(VulnerablePopulation $vulnerablePopulation)
    {
        //
    }
}
