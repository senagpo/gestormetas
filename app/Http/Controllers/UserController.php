<?php

namespace App\Http\Controllers;

use App\Models\Validity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     *  Returns the authenticated user info
     *
     *  @return \Illuminate\Http\JsonResponse
     */
    public function fetchUser()
    {
        try {
            $user = Auth::user();
            $role = $user->role;
            $validity = Validity::currentLoading();
            $regional = $role->name === 'reguser' ? $user->regionalEntity : null;

            return response()->json([
                'success' => true, 'user' => $user, 'role' => $role,
                'regional' => $regional, 'validity' => $validity
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false, 'user' => null,
                'role' => null, 'regional' => null,
                'validity' => null, 'error' => $th->getMessage()
            ], 500);
            throw $th;
        }
    }
}
