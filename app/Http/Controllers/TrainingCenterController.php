<?php

namespace App\Http\Controllers;

use App\Models\Goal;
use App\Models\Quota;
use App\Models\QuotaType;
use App\Models\TrainingCenter;
use App\Models\State;
use App\Models\Validity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TrainingCenterController extends Controller
{
    public function allTrainingCenters()
    {
        try {
            $trainingCenters = TrainingCenter::where('state_id', State::firstWhere('name', 'Activo')->id)
                ->where('regional_entity_id', Auth::user()->regionalEntity->id)
                ->get();
            return response()->json(['success' => true, 'trainingCenters' => $trainingCenters]);
        } catch (\Throwable $th) {
            return response()->json(['success' => false, 'trainingCenters' => null, 'error' => $th->getMessage()], 500);
        }
    }

    public function populationsRemaining()
    {
        try {
            $trainingCenters = TrainingCenter::where('state_id', State::firstWhere('name', 'Activo')->id)
                ->where('regional_entity_id', Auth::user()->regionalEntity->id)
                ->get();

            foreach ($trainingCenters as $trainingCenter) {

                $registeredQuotas = Quota::where('quota_type_id', QuotaType::firstWhere('name', 'Cuota de poblacion vulnerable')->id)
                    ->where('training_center_id', $trainingCenter->id)
                    ->get();

                foreach ($registeredQuotas as $quota) {
                    $goal = $quota->goal;
                    if ($goal->validity->id !== Validity::currentLoading()->id)
                        $registeredQuotas = $registeredQuotas->where('goal_id', '!=', $goal->id);
                }

                if ($registeredQuotas->count() > 0) $trainingCenters = $trainingCenters->where('id', '!=', $trainingCenter->id);
            }
            return response()->json(['success' => true, 'trainingCenters' => $trainingCenters]);
        } catch (\Throwable $th) {
            return response()->json(['success' => false, 'trainingCenters' => null, 'error' => $th->getMessage()], 500);
        }
    }

    public function technicRemaining()
    {
        try {
            $trainingCenters = TrainingCenter::where('state_id', State::firstWhere('name', 'Activo')->id)
                ->where('regional_entity_id', Auth::user()->regionalEntity->id)
                ->get();

            foreach ($trainingCenters as $trainingCenter) {
                $types = QuotaType::whereIn('name', [
                    'Cuota de Articulacion con la Media Nuevos',
                    'Cuota de Articulacion con la Media Pasan', 'Cuota de Articulacion con la Media Nuevos Academica',
                    'Cuota de Articulacion con la Media Nuevos Tecnica', 'Cuota de Articulacion con la Media Nuevos Privada',
                    'Cuota de Articulacion con la Media Pasan Academica', 'Cuota de Articulacion con la Media Pasan Tecnica',
                    'Cuota de Articulacion con la Media Pasan Privada'
                ])->pluck('id')->toArray();

                $registeredQuotas = Quota::whereIn('quota_type_id', $types)
                    ->where('training_center_id', $trainingCenter->id)
                    ->get();

                foreach ($registeredQuotas as $quota) {
                    $goal = $quota->goal;
                    if ($goal->validity->id !== Validity::currentLoading()->id)
                        $registeredQuotas = $registeredQuotas->where('goal_id', '!=', $goal->id);
                }

                if ($registeredQuotas->count() > 0) $trainingCenters = $trainingCenters->where('id', '!=', $trainingCenter->id);
            }
            return response()->json(['success' => true, 'trainingCenters' => $trainingCenters]);
        } catch (\Throwable $th) {
            return response()->json(['success' => false, 'trainingCenters' => null, 'error' => $th->getMessage()], 500);
        }
    }

    public function formationRemaining()
    {
        try {
            $trainingCenters = TrainingCenter::where('state_id', State::firstWhere('name', 'Activo')->id)
                ->where('regional_entity_id', Auth::user()->regionalEntity->id)
                ->get();

            foreach ($trainingCenters as $trainingCenter) {
                $types = QuotaType::whereIn('name', [
                    'Cuota de Formacion Titulada Pasan', 'Cuota de Formacion Titulada Nuevos',
                    'Cuota de Formacion Titulada Nuevos FIC', 'Cuota de Formacion Titulada Pasan FIC',
                    'Cuota de Formacion Titulada Nuevos FIC T.I', 'Cuota de Formacion Titulada Nuevos FIC T.II',
                    'Cuota de Formacion Titulada Nuevos FIC T.III', 'Cuota de Formacion Titulada Nuevos FIC T.IV',
                    'Cuota de Formacion Titulada Nuevos Regular', 'Cuota de Formacion Titulada Pasan Regular',
                    'Cuota de Formacion Titulada Nuevos Regular T.I', 'Cuota de Formacion Titulada Nuevos Regular T.II',
                    'Cuota de Formacion Titulada Nuevos Regular T.III', 'Cuota de Formacion Titulada Nuevos Regular T.IV',
                ])->pluck('id')->toArray();

                $registeredQuotas = Quota::whereIn('quota_type_id', $types)
                    ->where('training_center_id', $trainingCenter->id)
                    ->get();

                foreach ($registeredQuotas as $quota) {
                    $goal = $quota->goal;
                    if ($goal->validity->id !== Validity::currentLoading()->id)
                        $registeredQuotas = $registeredQuotas->where('goal_id', '!=', $goal->id);
                }

                if ($registeredQuotas->count() > 0) $trainingCenters = $trainingCenters->where('id', '!=', $trainingCenter->id);
            }
            return response()->json(['success' => true, 'trainingCenters' => $trainingCenters]);
        } catch (\Throwable $th) {
            return response()->json(['success' => false, 'trainingCenters' => null, 'error' => $th->getMessage()], 500);
        }
    }
}
