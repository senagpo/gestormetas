<?php

namespace App\Http\Controllers;

use App\Models\Budget;
use App\Models\BudgetType;
use App\Models\TrainingCenter;
use App\Models\Validity;
use Illuminate\Http\Request;

class BudgetController extends Controller
{
    public function storeBudget(Request $request, int $id)
    {
        try {
            $validity = Validity::currentLoading();
            $trainingCenter = TrainingCenter::findOrFail($id);
            $budgetTypes = BudgetType::all();

            $newKind = $request->new;
            $passKind = $request->pass;

            foreach ($budgetTypes as $budgetType) {
                $kind = $budgetType->kind;
                switch ($budgetType->name) {
                    case 'Formacion Regular':
                        $amount = $kind === 'Nuevos' ? $newKind['regular'] : $passKind['regular'];
                        Budget::updateOrCreate([
                            'validity_id' => $validity->id, 'training_center_id' => $trainingCenter->id,
                            'budget_type_id' => $budgetType->id
                        ], ['requested_amount' => $amount]);
                        break;
                    case 'Formacion Fic':
                        $amount = $kind === 'Nuevos' ? $newKind['fic'] : $passKind['fic'];
                        Budget::updateOrCreate([
                            'validity_id' => $validity->id, 'training_center_id' => $trainingCenter->id,
                            'budget_type_id' => $budgetType->id
                        ], ['requested_amount' => $amount]);
                        break;
                    case 'Articulacion con la Media':
                        $amount = $kind === 'Nuevos' ? $newKind['technic'] : $passKind['technic'];
                        Budget::updateOrCreate([
                            'validity_id' => $validity->id, 'training_center_id' => $trainingCenter->id,
                            'budget_type_id' => $budgetType->id
                        ], ['requested_amount' => $amount]);
                        break;
                    case 'Jovenes Rurales':
                        $amount = $newKind['ruralYouth'];
                        Budget::updateOrCreate([
                            'validity_id' => $validity->id, 'training_center_id' => $trainingCenter->id,
                            'budget_type_id' => $budgetType->id
                        ], ['requested_amount' => $amount]);
                        break;
                    case 'Desplazados':
                        $amount = $newKind['displaced'];
                        Budget::updateOrCreate([
                            'validity_id' => $validity->id, 'training_center_id' => $trainingCenter->id,
                            'budget_type_id' => $budgetType->id
                        ], ['requested_amount' => $amount]);
                        break;
                }
            }

            return response()->json(['success' => true]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'error' => $th->getMessage()
            ], 500);
            throw $th;
        }
    }
}
