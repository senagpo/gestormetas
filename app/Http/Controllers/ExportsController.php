<?php

namespace App\Http\Controllers;

use App\Exports\GeneralQuotasExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel as BaseExcel;
use Maatwebsite\Excel\Facades\Excel;

class ExportsController extends Controller
{
    public function downloadGeneral()
    {
        return Excel::download(new GeneralQuotasExport(), "ReporteGeneral.xlsx", BaseExcel::XLSX);
    }
}
