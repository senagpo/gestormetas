<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    /**
     *  Authenticates credentials and gives login
     *  token session
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function signIn(Request $request)
    {
        try {
            $name = $request->name;
            $password = $request->password;
            $remember = $request->remember;

            if (Auth::attempt(['name' => $name, 'password' => $password, 'state_id' => 1], $remember)) {
                $request->session()->regenerate(true);
                return response()->json([
                    'success' => true,
                    'user' => Auth::user(),
                    'role' => Auth::user()->role
                ]);
            } else {
                return response()->json(['success' => false], 401);
            }
        } catch (\Throwable $th) {
            return response()->json(['success' => false, 'error' => $th->getMessage()], 500);
            throw $th;
        }
    }

    /**
     *  Revokes current session and logout the
     *  user
     *
     *  @return \Illuminate\Http\JsonResponse
     */
    public function signOut()
    {
        if (Auth::check()) {
            Auth::guard('web')->logout();
            Session::invalidate();
        }
        return response()->json(['success' => true]);
    }

    /**
     *  Changes the current user password
     *
     *  @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request)
    {
        try {
            $user = Auth::user();
            $nowPass = $request->now;
            $newPass = $request->new;
            if (!Hash::check($nowPass, $user->password)) return response()->json(['success' => false]);
            $user->password = Hash::make($newPass);
            if ($user->saveOrFail()) {
                return response()->json(['success' => true]);
            } else {
                return response()->json(['success' => false]);
            }
        } catch (\Throwable $th) {
            return response()->json(['success' => false], 500);
            throw $th;
        }
    }
}
