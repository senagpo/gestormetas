<?php

namespace App\Http\Controllers;

use App\Models\Validity;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;

class ValidityController extends Controller
{
    /**
     *  Returns all registered validities
     *
     *  @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        try {
            $validities = Validity::all();

            return response()->json(['success' => true, 'validities' => $validities]);
        } catch (\Throwable $th) {
            return response()->json(['success' => false, 'validities' => null, 'error' => $th->getMessage()], 500);
        }
    }

    /**
     *  Performs the change of the current active validity
     *
     *  @return \Illuminate\Http\JsonResponse
     */
    public function changeValidity(int $id)
    {
        try {
            $currentValidity = Validity::currentLoading();
            $toChangeValidity = Validity::findOrFail($id);
            $nowDate = Carbon::now('America/Bogota');

            if ($currentValidity->id === $toChangeValidity->id) return response()->json(['success' => false]);

            $currentValidity->can_load = false;
            $currentValidity->can_view = true;
            $currentValidity->end_date = $nowDate;
            $currentValidity->duration = $currentValidity->start_date->diffInMonths($nowDate, true);

            $toChangeValidity->can_load = true;

            if ($currentValidity->saveOrFail() && $toChangeValidity->saveOrFail()) return response()->json(['success' => true]);
            else return response()->json(['success' => false]);
        } catch (\Throwable $th) {
            return response()->json(['success' => false, 'error' => $th->getMessage()], 500);
        }
    }
}
