<?php

namespace App\Exports;

use App\Models\Budget;
use App\Models\BudgetType;
use App\Models\Quota;
use App\Models\QuotaType;
use App\Models\TrainingCenter;
use App\Models\Validity;
use App\Models\VulnerablePopulation;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithTitle;

class GeneralQuotasExport implements FromCollection, ShouldAutoSize, WithHeadings, WithTitle, WithStrictNullComparison
{

    private $validity;

    public function __construct()
    {
        $this->validity = Validity::currentLoading()->start_date->year;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data = collect();

        $trainingCenters = TrainingCenter::all();

        $validity = Validity::currentLoading();

        foreach ($trainingCenters as $trainingCenter) {
            $row = [];
            /** Filling Training Center data */
            $regionalEntity = $trainingCenter->regionalEntity;
            $row[] = $regionalEntity->code;
            $row[] = $regionalEntity->name;
            $row[] = $trainingCenter->code;
            $row[] = $trainingCenter->name;

            /** Filling Vulnerable Population data */
            $populations = VulnerablePopulation::all();

            foreach ($populations as $population) {
                /**
                 * @var Quota|null
                 */
                $quota = Quota::where('training_center_id', $trainingCenter->id)
                    ->where('quota_type_id', QuotaType::firstWhere('name', 'Cuota de poblacion vulnerable')->id)
                    ->where('vulnerable_population_id', $population->id)
                    ->where('validity_id', $validity->id)
                    ->first();
                $row[] = isset($quota) ? $quota->places_amount : 'No se ha registrado desagregacion';
                $row[] = isset($quota) ? $quota->apprentices_amount : 'No se ha registrado desagregacion';
            }

            /** Filling Technical data */
            $technicalModes = ['Total', 'Academica', 'Tecnica', 'Privada'];

            foreach ($technicalModes as $mode) {
                foreach (['Nuevos', 'Pasan'] as $type) {
                    $typeSearch = $mode !== 'Total' ?  "Cuota de Articulacion con la Media {$type} {$mode}" : "Cuota de Articulacion con la Media {$type}";
                    /**
                     * @var Quota|null
                     */
                    $quota = Quota::where('training_center_id', $trainingCenter->id)
                        ->where('quota_type_id', QuotaType::firstWhere('name', $typeSearch)->id)
                        ->where('validity_id', $validity->id)
                        ->first();
                    $row[] = isset($quota) ? $quota->places_amount : 'No se ha registrado desagregacion';
                    $row[] = isset($quota) ? $quota->apprentices_amount : 'No se ha registrado desagregacion';
                }
            }

            /** Filling Formation data */

            $formationModes = ['Total', 'Regular', 'FIC'];
            $periods = ['T.I', 'T.II', 'T.III', 'T.IV'];
            foreach ($formationModes as $mode) {
                foreach (['Nuevos', 'Pasan'] as $type) {
                    $typeSearch = $mode !== 'Total' ?  "Cuota de Formacion Titulada {$type} {$mode}" : "Cuota de Formacion Titulada {$type}";
                    /**
                     * @var Quota|null
                     */
                    $quota = Quota::where('training_center_id', $trainingCenter->id)
                        ->where('quota_type_id', QuotaType::firstWhere('name', $typeSearch)->id)
                        ->where('validity_id', $validity->id)
                        ->first();

                    $row[] = isset($quota) ? $quota->places_amount : 'No se ha registrado desagregacion';
                    if ($mode !== 'Total' && $type !== 'Pasan') {
                        foreach ($periods as $period) {
                            $typeSearch = "Cuota de Formacion Titulada {$type} {$mode} {$period}";
                            /**
                             * @var Quota|null
                             */
                            $quota = Quota::where('training_center_id', $trainingCenter->id)
                                ->where('quota_type_id', QuotaType::firstWhere('name', $typeSearch)->id)
                                ->where('validity_id', $validity->id)
                                ->first();
                            $row[] = isset($quota) ? $quota->places_amount : 'No se ha registrado desagregacion';
                        }
                    }
                }
            }


            /** Filling Budget Data */

            $budgetTypes = [
                'Nuevos' => [
                    'Formacion Regular', 'Formacion Fic', 'Articulacion con la Media',
                    'Jovenes Rurales', 'Desplazados'
                ],
                'Pasan' => [
                    'Formacion Regular', 'Formacion Fic', 'Articulacion con la Media',
                ],
            ];

            foreach ($budgetTypes as $kind => $names) {
                foreach ($names as $name) {
                    /**
                     * @var BudgetType|null
                     */
                    $type = BudgetType::where('name', $name)->where('kind', $kind)->first();
                    /**
                     * @var Budget|null
                     *
                     */
                    $budget = Budget::where('training_center_id', $trainingCenter->id)
                        ->where('budget_type_id', $type->id)
                        ->where('validity_id', $validity->id)
                        ->first();
                    $row[] = isset($budget) ? $budget->requested_amount : 'No hay solicitud presupuestal registrada';
                }
            }


            $data->push($row);
        }

        return $data;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Vigencia ' . $this->validity;
    }

    public function headings(): array
    {

        $titles = ['Codigo Regional', 'Regional', 'Codigo Centro de formacion', 'Centro de formacion'];

        $populations = VulnerablePopulation::all();

        foreach ($populations as $population) {
            $titles[] = "Cupos {$population->name} Vigencia {$this->validity}";
            $titles[] = "Aprendices {$population->name} Vigencia {$this->validity}";
        }


        $technicalModes = ['Total', 'Academica', 'Tecnica', 'Privada'];


        foreach ($technicalModes as $mode) {
            foreach (['Nuevos', 'Pasan'] as $type) {
                $titles[] = "Cupos Articulacion con la media {$mode} {$type}";
                $titles[] = "Aprendices Articulacion con la media {$mode} {$type}";
            }
        }

        $formationModes = ['Total', 'Regular', 'FIC'];
        $periods = ['T.I', 'T.II', 'T.III', 'T.IV'];
        foreach ($formationModes as $mode) {
            foreach (['Nuevos', 'Pasan'] as $type) {
                $titles[] = "Cupos Formacion Titualada {$mode} {$type}";
                if ($mode !== 'Total' && $type !== 'Pasan') {
                    foreach ($periods as $period) {
                        $titles[] = "Cupos Formacion Titualada {$mode} {$type} {$period}";
                    }
                }
            }
        }

        $budgetTypes = [
            'Nuevos' => [
                'Formacion Regular', 'Formacion Fic', 'Articulacion con la Media',
                'Jovenes Rurales', 'Desplazados'
            ],
            'Pasan' => [
                'Formacion Regular', 'Formacion Fic', 'Articulacion con la Media',
            ],
        ];

        foreach ($budgetTypes as $kind => $types) {
            foreach ($types as $type) {
                $titles[] = "Solicitud Presupuestal para {$type} {$kind}";
            }
        }

        return $titles;
    }
}
