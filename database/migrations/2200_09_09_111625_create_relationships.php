<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('state_id')->references('id')->on('states');
        });

        Schema::table('regional_entities', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('state_id')->references('id')->on('states');
        });

        Schema::table('training_centers', function (Blueprint $table) {
            $table->foreign('regional_entity_id')->references('id')->on('regional_entities');
            $table->foreign('state_id')->references('id')->on('states');
        });

        Schema::table('goals', function (Blueprint $table) {
            $table->foreign('regional_entity_id')->references('id')->on('regional_entities');
            $table->foreign('training_center_id')->references('id')->on('training_centers');
            $table->foreign('vulnerable_population_id')->references('id')->on('vulnerable_populations');
            $table->foreign('goal_type_id')->references('id')->on('goal_types');
            $table->foreign('validity_id')->references('id')->on('validities');
            $table->foreign('state_id')->references('id')->on('states');
        });

        Schema::table('quotas', function (Blueprint $table) {
            $table->foreign('goal_id')->references('id')->on('goals');
            $table->foreign('training_center_id')->references('id')->on('training_centers');
            $table->foreign('validity_id')->references('id')->on('validities');
            $table->foreign('vulnerable_population_id')->references('id')->on('vulnerable_populations');
            $table->foreign('quota_type_id')->references('id')->on('quota_types');
        });

        Schema::table('budgets', function (Blueprint $table) {
            $table->foreign('validity_id')->references('id')->on('validities');
            $table->foreign('training_center_id')->references('id')->on('training_centers');
            $table->foreign('budget_type_id')->references('id')->on('budget_types');
        });
    }
}
