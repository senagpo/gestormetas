<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goals', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('places');
            $table->bigInteger('apprentices');
            $table->foreignId('validity_id');
            $table->foreignId('state_id');
            $table->foreignId('goal_type_id');
            $table->foreignId('regional_entity_id')->nullable()->default(null);
            $table->foreignId('training_center_id')->nullable()->default(null);
            $table->foreignId('vulnerable_population_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goals');
    }
}
