<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('places_amount');
            $table->bigInteger('apprentices_amount');
            $table->foreignId('quota_type_id');
            $table->foreignId('goal_id');
            $table->foreignId('training_center_id');
            $table->foreignId('validity_id');
            $table->foreignId('vulnerable_population_id')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotas');
    }
}
