<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValiditiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('validities', function (Blueprint $table) {
            $table->id();
            $table->string('name', 250);
            $table->integer('duration')->default(12);
            $table->date('start_date')->default(today('America/Bogota')->startOfYear());
            $table->date('end_date')->default(today('America/Bogota')->endOfYear());
            $table->boolean('can_load')->default(true);
            $table->boolean('can_view')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('validities');
    }
}
