<?php

namespace Database\Seeders;

use App\Models\VulnerablePopulation;
use Illuminate\Database\Seeder;

class VulnerablePopulationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vulnerablePopulations = [
            'Discapacitados', 'Negritudes', 'Afrocolombianos', 'Raizales',
            'Palenqueros', 'Desplazados por Fenómenos Naturales', 'Indígenas',
            'INPEC', 'Jóvenes Vulnerables', 'Adolescente en Conflicto con la Ley Penal',
            'Mujer Cabeza de Hogar', 'Proceso de Reintegración y Adolescentes desvinculados de Grupos Armados Organizados al margen de la ley',
            'Tercera Edad', 'Adolescente Trabajador', 'Remitidos por el PAL',
            'Soldados Campesinos', 'Sobrevivientes Minas Antipersonales', 'Rroom'
        ];

        try {
            foreach ($vulnerablePopulations as $populationName) {
                $population = new VulnerablePopulation();
                $population->name = $populationName;
                $population->saveOrFail();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
