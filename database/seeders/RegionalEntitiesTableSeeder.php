<?php

namespace Database\Seeders;

use App\Models\RegionalEntity;
use App\Models\State;
use App\Models\User;
use Illuminate\Database\Seeder;

class RegionalEntitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fullPath = storage_path('app/data/RegionalEntitiesList.json');
        try {
            $regionalList = json_decode(file_get_contents($fullPath));
            foreach ($regionalList as $regionalData) {
                $regional = new RegionalEntity();
                $regional->code = $regionalData->code;
                $regional->name = $regionalData->name;
                $regional->user_id = User::all()->firstWhere('email', '=', $regionalData->email)->id;
                $regional->state_id = State::all()->firstWhere('name', '=', 'Activo')->id;
                $regional->saveOrFail();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
