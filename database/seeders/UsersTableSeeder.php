<?php

namespace Database\Seeders;

use App;
use App\Models\Role;
use App\Models\State;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fullPath = storage_path('app/data/RegionalEntitiesList.json');
        $fullPathUsers = storage_path('app/data/UsersList.json');
        try {
            $adminUser = new User();
            $adminUser->name = 'AdminGPO';
            $adminUser->email = env('ADMIN_EMAIL');
            $adminUser->password = Hash::make(env('ADMIN_PASSWORD'));
            $adminUser->role_id = Role::firstWhere('name', 'admin')->id;
            $adminUser->state_id = State::firstWhere('name', '=', 'Activo')->id;
            $adminUser->saveOrFail();

            $adminUser = new User();
            $adminUser->name = 'managerGPO1';
            $adminUser->email = '';
            $adminUser->password = Hash::make('managerGPO1');
            $adminUser->role_id = Role::firstWhere('name', 'manager')->id;
            $adminUser->state_id = State::firstWhere('name', '=', 'Activo')->id;
            $adminUser->saveOrFail();

            $regionalList = json_decode(file_get_contents($fullPath));
            foreach ($regionalList as $regionalData) {
                $regionalData = $regionalData;
                $regionalUser = new User();
                $regionalUser->name = "Reg{$regionalData->code}";
                $regionalUser->email = $regionalData->email;
                if (App::environment('local')) {
                    $regionalUser->password = Hash::make("regpass{$regionalData->code}");
                } elseif (App::environment('staging')) {
                    $regionalUser->password = Hash::make("reg{$regionalData->code}pass{$regionalData->code}STG");
                }
                $regionalUser->role_id = Role::firstWhere('name', 'reguser')->id;
                $regionalUser->state_id = State::firstWhere('name', 'Activo')->id;
                $regionalUser->saveOrFail();
            }
            $usersList = json_decode(file_get_contents($fullPathUsers));
            foreach ($usersList as $userJS) {
                $user = new User();
                $user->name = $userJS->User;
                $user->email = $userJS->Email;
                $user->password = Hash::make("{$userJS->Name}pass2020");
                $user->role_id = Role::firstWhere('name', $userJS->Role)->id;
                $user->state_id = State::firstWhere('name', '=', 'Activo')->id;
                $user->saveOrFail();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
