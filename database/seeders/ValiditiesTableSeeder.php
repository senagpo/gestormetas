<?php

namespace Database\Seeders;

use App\Models\Validity;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class ValiditiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nowDate = Carbon::now('America/Bogota');
        $lastValidity = 2099;
        $validityYears = range(2021, $lastValidity);
        try {
            foreach ($validityYears as $validityYear) {
                $validityYear = Carbon::createFromDate($validityYear, 1, 1);
                $validityName = "Vigencia {$validityYear->year}";
                $validityDuration = $validityYear->diffInMonths($validityYear->copy()->addYear(), true);
                $validityStartDate = $validityYear->copy()->startOfYear();
                $validityEndDate = $validityYear->copy()->endOfYear();
                $validityCanLoad = $validityYear->year === $nowDate->copy()->addYear()->year ? true : false;
                $validityCanView = $validityYear->year < $nowDate->year ? true : false;

                $validity = new Validity();
                $validity->name = $validityName;
                $validity->duration = $validityDuration;
                $validity->start_date = $validityStartDate;
                $validity->end_date = $validityEndDate;
                $validity->can_load = $validityCanLoad;
                $validity->can_view = $validityCanView;
                $validity->saveOrFail();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
