<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleNames = ['Administrador' => 'admin', 'Gestor Operativo' => 'manager', 'Usuario Regional' => 'reguser'];
        try {
            foreach ($roleNames as $display => $name) {
                $role = new Role();
                $role->display = $display;
                $role->name = $name;
                $role->saveOrFail();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
