<?php

namespace Database\Seeders;

use App\Models\BudgetType;
use App\Models\GoalType;
use App\Models\QuotaType;
use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $goalTypeNames = [
            'Meta Regional para Poblacion Vulnerable', 'Meta Regional para Articulacion con la Media',
            'Meta Regional para Formacion Titulada', 'Meta Centro para Poblacion Vulnerable',
            'Meta Centro para Articulacion con la media', 'Meta Centro para Formacion Titulada'
        ];
        $quotaTypeNames = [
            'Cuota de poblacion vulnerable', 'Cuota de Articulacion con la Media Nuevos',
            'Cuota de Articulacion con la Media Pasan', 'Cuota de Articulacion con la Media Nuevos Academica',
            'Cuota de Articulacion con la Media Nuevos Tecnica', 'Cuota de Articulacion con la Media Nuevos Privada',
            'Cuota de Articulacion con la Media Pasan Academica', 'Cuota de Articulacion con la Media Pasan Tecnica',
            'Cuota de Articulacion con la Media Pasan Privada',

            'Cuota de Formacion Titulada Pasan', 'Cuota de Formacion Titulada Nuevos',
            'Cuota de Formacion Titulada Nuevos FIC', 'Cuota de Formacion Titulada Pasan FIC',
            'Cuota de Formacion Titulada Nuevos FIC T.I', 'Cuota de Formacion Titulada Nuevos FIC T.II',
            'Cuota de Formacion Titulada Nuevos FIC T.III', 'Cuota de Formacion Titulada Nuevos FIC T.IV',
            'Cuota de Formacion Titulada Nuevos Regular', 'Cuota de Formacion Titulada Pasan Regular',
            'Cuota de Formacion Titulada Nuevos Regular T.I', 'Cuota de Formacion Titulada Nuevos Regular T.II',
            'Cuota de Formacion Titulada Nuevos Regular T.III', 'Cuota de Formacion Titulada Nuevos Regular T.IV',
        ];

        $budgetTypes = [
            'Nuevos' => [
                'Formacion Regular', 'Formacion Fic', 'Articulacion con la Media',
                'Jovenes Rurales', 'Desplazados'
            ],
            'Pasan' => [
                'Formacion Regular', 'Formacion Fic', 'Articulacion con la Media',
            ],
        ];

        try {
            foreach ($goalTypeNames as $goalName) {
                $goalType = new GoalType();
                $goalType->name = $goalName;
                $goalType->saveOrFail();
            }

            foreach ($quotaTypeNames as $quotaName) {
                $quotaType = new QuotaType();
                $quotaType->name = $quotaName;
                $quotaType->saveOrFail();
            }

            foreach ($budgetTypes as $kind => $types) {
                foreach ($types as $type) {
                    $budgetType = new BudgetType();
                    $budgetType->name = $type;
                    $budgetType->kind = $kind;
                    $budgetType->saveOrFail();
                }
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
