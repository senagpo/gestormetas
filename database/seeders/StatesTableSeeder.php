<?php

namespace Database\Seeders;

use App\Models\State;
use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stateNames = ['Activo', 'Inactivo', 'Desagregado', 'Sin Desagregar'];
        try {
            foreach ($stateNames as $name) {
                $state = new State();
                $state->name = $name;
                $state->saveOrFail();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
