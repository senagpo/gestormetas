<?php

namespace Database\Seeders;

use App\Models\RegionalEntity;
use App\Models\State;
use App\Models\TrainingCenter;
use Illuminate\Database\Seeder;

class TrainingCentersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fullPath = storage_path('app/data/TrainingCentersList.json');
        try {
            $trainingCentersList = json_decode(file_get_contents($fullPath));
            foreach ($trainingCentersList as $trainingCenter) {
                $center = new TrainingCenter();
                $center->code = $trainingCenter->code;
                $center->name = $trainingCenter->name;
                $center->regional_entity_id = RegionalEntity::all()->firstWhere('code', '=', $trainingCenter->regional_code)->id;
                $center->state_id = State::all()->firstWhere('name', '=', 'Activo')->id;
                $center->saveOrFail();
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
